#!/bin/bash

# pacman -Sy git
mkdir -p /mnt/home/$user/Desktop
cd /mnt/home/$user/Desktop

git clone https://gitlab.com/shastenm76/arch

chmod 777 arch

cd arch

chmod +x arch_install_1.sh
chmod +x arch_install_2.sh
chmod +x arch_install_3.sh
chmod 777 pacman.conf
chmod 777 sudoers
chmod 777 grub
chmod 777 lightdm.conf
chmod 777 display_setup
chmod 777 xmonad
chmod +x xmonad/xmonad.sh

./arch_install_1.sh
