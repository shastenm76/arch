#!/bin/bash

sudo git clone https://aur.archlinux.org/package-query
sudo git clone https://aur.archlinux.org/yaourt
sudo git clone https://aur.archlinux.org/yay

sudo chmod 777 package-query
sudo chmod 777 yaourt 
sudo chmod 777 yay

cd package-query
makepkg -si

cd ../yaourt
makepkg -si

cd ../yay
makepkg -si

cd /home/Desktop/arch