#!/bin/bash

mkdir -p ~/.xmonad
sudo chmod 777 ~/.xmonad
cd ~/.xmonad

sudo ln -s ~/.local/bin/xmonad /usr/bin

sudo curl -sSL https://get.haskellstack.org/ | sh

sudo stack setup

sudo git clone "https://github.com/xmonad/xmonad" xmonad-git
sudo git clone "https://github.com/xmonad/xmonad-contrib" xmonad-contrib-git
sudo git clone "https://github.com/jaor/xmobar" xmobar-git

sudo stack init
cd ~/.xmonad
sudo stack install --allow-different-user

sudo cp -r -i -v /home/Desktop/arch/xmonad/build.bak ~/.xmonad/build
sudo chmod a+x ~/.xmonad/build

sudo touch /usr/share/xsessions/xmonad.desktop
sudo chmod 777 /usr/share/xsessions/xmonad.desktop
sudo cp -r -i -v /home/Desktop/arch/xmonad/xmonad.desktop.bak /usr/share/xsessions/xmonad.desktop

sudo touch ~/.xmonad/xmonad.hs
sudo cp -r -i -v /home/Desktop/arch/xmonad/xmonad.hs.bak ~/.xmonad/xmonad.hs
sudo touch ~/.xmonad/xmobarrc0
sudo cp -r -i -v /home/Desktop/arch/xmonad/xmobarrc0.bak ~/.xmonad/xmobarrc0
sudo touch ~/.xmonad/xmobarrc1 
sudo cp -r -i -v /home/Desktop/arch/xmonad/xmobarrc1.bak ~/.xmonad/xmobarrc1
sudo touch ~/.xmonad/xmobarrc2
sudo cp -r -i -v /home/Desktop/arch/xmonad/xmobarrc2.bak ~/.xmonad/xmobarrc2
sudo touch ~/xmonad/xmonad.start
sudo cp -r -i -v /home/Desktop/arch/xmonad/xmonad.start.bak ~/.xmonad/xmonad.start

##//Tiene que recompilar despues de hacer cambios//
sudo xmonad --recompile && sudo xmonad --restart

