#!/bin/bash


cp -r -i -v pacman.conf /etc/pacman.conf
cp -r -i -v sudoers /etc/sudoers

source /etc/pacman.conf
source /etc/sudoers

# install most of the base pkgs

pacman -Sy git wget curl

# Hostname--put any hostname you wish

echo arch-geek > /etc/hostname
passwd

# Username 

useradd -m -G audio,disk,lp,optical,storage,video,wheel,games,power,scanner -s /bin/bash shasten
passwd 

# install grub

grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg

exit 
umount -R /mnt
